# git_group_practice

This project is for group practice regarding git `rebase`, `merge` and `commit messages`.

## Making a repo and branching it

Initially we have made a dev branch out of which we branched three feature branches individually.

## Made some conflicting changes in each feature branch

Made conflicting chnages and then rebased and merged those branches in development again as follows:

- Made changes in feature branch
- Rebased development branch into feature branch with `git checkout <feature_branch>` then `git rebase <development_branch>`
- Resolved conflicts
- Then merged feature feature branch into development


